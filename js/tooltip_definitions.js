/*
 * @file
 * Adds functionality for tooltip, as well as pulls in callback content, for
 * the Tooltip Definitions module.
 */
(function ($) {
  $(document).ready(function() {
      $('.define-me').each(function() {
         var term = encodeURIComponent($(this).html());
         $(this).append('<div class="tooltip hide"></div>');  
         $(this).find('.tooltip').load(window.location.protocol + '//' + window.location.host + '/tooltip_definitions/callback/' + term, function (response, status, thevar){
           //in case there is no node for that term, we remove the tooltip html entirely
           if (response.length < 1){
             $(this).parent().removeClass('define-me');
             $(this).remove();
           }       
         });
      });     
      $('.define-me').each(function(){ 
        $(this).click(function(){
          if ($(this).find('.tooltip').hasClass('show')){
            $('.tooltip').removeClass('show').addClass('hide'); 
          }
          else{
            $('.tooltip').removeClass('show').addClass('hide'); 
            $(this).find('.tooltip').addClass('show').removeClass('hide'); 
          } 
        });
      });
 });
})(jQuery);