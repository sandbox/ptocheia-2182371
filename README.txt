
The Tooltip Definitions module makes it easy add tooltips to selected words. The module works by creating a definition content type, consisting of a title field (which holds the word being defined_ and a body field (named "definition", which holds the definition for the term in the title field). Use the definition content type to add terms and their definitions to the site. Then, whenever on the site you would like to have one of these terms defined, wrap the term in an html tag with that class "define-me". This will highlight the term, and pull the definition for that term into a tooltip that opens by selecting the term. 

The manual wrapping of terms (versus auto-finding those terms and creating tooltips for them) gives higher control for usage of the tooltip - if a term is used several times, the admin can choose which instance(s) have tooltips applied to them. 

Installation
------------

Copy the tooltip_definitions directory to your module directory and then enable 
on the admin modules page. 

Author
------
Virginia Roper
moi@varoper.com